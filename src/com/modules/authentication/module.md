Module: Authentication
Description: This is the authenticate user module this allows someone to login and will set the information into a cookie using the base64 command to obfuscate the information sent. This also sets a global auth key and username that is used on the home screen as a test.  
Author: Ray Bayly
Date: 8/13/2015


Controller: LoginController
Service: AuthenticationService
Directives: None
Dependencies: Base64, 
Views: login.html 


*********************************************************************************************
Notes: 

Route Config points to /login
Controller Takes the login information (User/Pass) and then sets that information into global variables and also the cookie, it will base64 the information , The controller will send the information to the service to validate it and return the auth key. 
