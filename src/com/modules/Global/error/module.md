Module: About
Description: This is the about page view module and is used to show about the website. 
Author: Ray Bayly
Date: 8/13/2015


Controller: aboutController
Service: None
Directives: None
Dependencies: None
Views: about.html 


*********************************************************************************************
Notes: 

Route Config points to /about
Controller Displays a message from scope.message
