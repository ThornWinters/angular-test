Module: Products.productList
Description: This is the page for product listing for all products.
Author: Michael Warner
Date: 9/26/2015


Controller: productListController
Service: productList
Directives: None
Dependencies: None
Views: productList.html


*********************************************************************************************
Notes: 

Route Config points to /productList
