'use strict';

angular.module('myApp.module.Products.productList.Controller', ['ngRoute'])// jshint ignore:line
    

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/productList', {
            controller: 'productListController',
            templateUrl: '/com/modules/Products/List/views/productList.html',
            hideMenus: true,
            protectedArea: false,
            title: 'Product List',
            menuGroup: 'Product List',
            description: 'This is the product listing',
            keywords: 'product,list',
            breadcrumbList: [{view: '/', title: 'Home'}, {view: '/productList', title: 'Product List'}],
            resolve: {
                productList: ['$http', function($http){
                    return $http.get('/rest/Products/listall.json').then(function(responce){
                        return responce;
                    });
                }]
            }
      });
    }])
    .controller('productListController', ['$scope','$location', 'productList', function($scope, $location ,productList) {
        
        $scope.products = productList.data.data;
        $scope.location = $location;
        //This will redirect the user also it will add the id of the item in the url so they can make shortcuts and share the product.
        $scope.redirect = function(id){
            var location = this.$parent.location;
            location.path('/productDetails/'+id);
        };

    }]);