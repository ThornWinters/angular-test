
describe('Unit: myApp.module.Products.productList.Controller', function() {
    
    beforeEach(function(){
        module('myApp.module.Products.productList.Controller');
        module(function ($provide) {
            $provide.value('productList', 
            {
                "data" : {
                    "data": [
                        {
                            "id": "12",
                            "name": "Karbolyn",
                            "pic": "/lib/img/demo4.jpg",
                            "price": "$12.95",
                            "description": "Pure beef protein isolate. 25g protein. Fat 0 grams."
                        }
                    ]
                }
            });
        });
    });
    
    var HomeController, scope;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();

        productListController = $controller('productListController', {
            $scope: scope
        });
    }));

    it('Testing Products data', function () {
        expect(scope.products[0].id).toEqual('12');
        expect(scope.products[0].name).toEqual('Karbolyn');
        expect(scope.products[0].pic).toEqual('/lib/img/demo4.jpg');
        expect(scope.products[0].price).toEqual('$12.95');
        expect(scope.products[0].description).toEqual('Pure beef protein isolate. 25g protein. Fat 0 grams.');
    });
    it('Testing the Route Information in the about Module',
        inject(function ($route) {
          expect($route.routes['/productList'].controller).toBe('productListController');
          expect($route.routes['/productList'].templateUrl).toEqual('/com/modules/Products/List/views/productList.html');
          expect($route.routes['/productList'].hideMenus).toBe(true);
          expect($route.routes['/productList'].protectedArea).toBe(false);
          expect($route.routes['/productList'].title).toBe('Product List');
          expect($route.routes['/productList'].description).toBe('This is the product listing');
          expect($route.routes['/productList'].keywords).toBe('product,list');
    }));
});


    
