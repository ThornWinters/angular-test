Module: Products.productDetails
Description: This is the Product Details module this is the page to get all the information about a single product.
Author: Michael Warner
Date: 8/13/2015


Controller: productDetailsController
Service: productInfo
Directives: None
Dependencies: None, 
Views: productDetails.html 


*********************************************************************************************
Notes: 

Route Config points to /productDetails/(productID)
