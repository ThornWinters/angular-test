'use strict';

angular.module('myApp.module.Products.productDetails.Controller', ['ngRoute'])// jshint ignore:line
    

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/productDetails/:productId', {
            controller: 'productDetailsController',
            templateUrl: '/com/modules/Products/Details/views/productDetails.html',
            hideMenus: true,
            protectedArea: false,
            title: 'Product Details',
            menuGroup: 'Product Details',
            description: 'This is the details on a single product',
            keywords: 'product,details',
            breadcrumbList: [{view: '/', title: 'Home'},{view: '/productList', title: 'Product List'}, {view: '/productDetails/', title: 'Product'}],
            resolve: {
                productInfo: ['$http', function($http){
                    return $http.get('/rest/Products/getProduct.json').then(function(responce){
                        return responce;
                    });
                }]
            }
      });
    }])

    .controller('productDetailsController', ['$scope','$location', 'productInfo', function($scope, $location, productInfo) {
        /*
            Correcting the breadCrumbs so they will point to the dynamic location set in the url.
            This is not the ideal solution it would be better if we could place it above in the 
            breadcumbList, but as we needed access to $location or something like it.
        */
        if($scope.breadCrumb){
            $scope.breadCrumb[2].view = '/productDetails/' + $location.$$url.split('/')[2];
        }
        //get product info
        $scope.product = productInfo.data.data[0];

        //create a variable and default value for the selected image.
        $scope.selectedPic = $scope.product.pic1;
    }]);