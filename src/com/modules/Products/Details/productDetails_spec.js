
describe('Unit: myApp.module.Products.productDetails.Controller', function() {
    
    beforeEach(function(){
        module('myApp.module.Products.productDetails.Controller');
        module(function ($provide) {
            $provide.value('productInfo', 
            {
                "data" : {
                    "data": [
                        {
                            "id": "12",
                            "name": "Karbolyn",
                            "catchline":"Bigger, Cooler, Faster",
                            "pic1": "/lib/img/demo4.jpg",
                            "pic2": "/lib/img/demo5.jpg",
                            "pic3": "/lib/img/demo6.jpg",
                            "pic4": "/lib/img/demo7.jpg",
                            "wholesale":"$82.45",
                            "price": "$12.95",
                            "description": "Karbolyn® represents the dawn of a new era of carbohydrate supplementation. Unlike other carbohydrate sources, Karbolyn® is quickly absorbed into the bloodstream like a simple sugar but without the typical spike or crash caused when your muscles run out of fuel.* In fact, Karbolyn® is a 'homopolysaccharide' (relatively complex carbohydrate), suggested to be absorbed 18.21% faster than pure Dextrose (a simple carbohydrate).* Yet it still promotes the sustained energy of complex carbohydrates.*"
                        }
                    ]
                }
            });
        });
    });
    
    var HomeController, scope;

    beforeEach(inject(function ($rootScope, $controller, $httpBackend) {
        scope = $rootScope.$new();

        productListController = $controller('productDetailsController', {
            $scope: scope
        });
    }));

    it('Testing Product data', function () {
        expect(scope.product.id).toEqual('12');
        expect(scope.product.name).toEqual('Karbolyn');
        expect(scope.product.catchline).toEqual("Bigger, Cooler, Faster");
        expect(scope.selectedPic).toEqual(scope.product.pic1);
        expect(scope.product.pic1).toEqual('/lib/img/demo4.jpg');
        expect(scope.product.pic2).toEqual('/lib/img/demo5.jpg');
        expect(scope.product.pic3).toEqual('/lib/img/demo6.jpg');
        expect(scope.product.pic4).toEqual('/lib/img/demo7.jpg');
        expect(scope.product.wholesale).toEqual("$82.45");
        expect(scope.product.price).toEqual('$12.95');
        expect(scope.product.description).toEqual('Karbolyn® represents the dawn of a new era of carbohydrate supplementation. Unlike other carbohydrate sources, Karbolyn® is quickly absorbed into the bloodstream like a simple sugar but without the typical spike or crash caused when your muscles run out of fuel.* In fact, Karbolyn® is a \'homopolysaccharide\' (relatively complex carbohydrate), suggested to be absorbed 18.21% faster than pure Dextrose (a simple carbohydrate).* Yet it still promotes the sustained energy of complex carbohydrates.*');
    });
    it('Testing the Route Information in the about Module',
        inject(function ($route) {
            expect($route.routes['/productDetails/:productId'].controller).toBe('productDetailsController');
            expect($route.routes['/productDetails/:productId'].templateUrl).toEqual('/com/modules/Products/Details/views/productDetails.html');
            expect($route.routes['/productDetails/:productId'].hideMenus).toBe(true);
            expect($route.routes['/productDetails/:productId'].protectedArea).toBe(false);
            expect($route.routes['/productDetails/:productId'].title).toBe('Product Details');
            expect($route.routes['/productDetails/:productId'].description).toBe('This is the details on a single product');
            expect($route.routes['/productDetails/:productId'].keywords).toBe('product,details');
    }));
});


    
