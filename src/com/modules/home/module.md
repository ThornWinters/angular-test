Module: Home
Description: This is the Home Page module this is the main page 
Author: Ray Bayly
Date: 8/13/2015


Controller: homeController
Service: None
Directives: showMessageWhenHovered
Dependencies: None, 
Views: home.html 


*********************************************************************************************
Notes: 

Route Config points to /
Controller sets scope.message and displays it on the main page, the view page has images that when you mouse over use the directive to change a message below them . 
