Module: Registration Confirmation
Description: This is the registration confirmation page, this page is set as the cofirmation for registration
Author: Ray Bayly
Date: 8/13/2015


Controller: rcController
Service: None
Directives: None
Dependencies: None
Views: about.html 


*********************************************************************************************
Notes: 

Route Config points to /registration-confirmation
Controller Displays a message from scope.message
