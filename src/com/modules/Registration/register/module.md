Module: Register
Description: This is the registration module, this module is used to register users on the system
Author: Ray Bayly
Date: 8/13/2015


Controller: registerController
Service: none
Directives: none
Dependencies: None, 
Views: register.html 


*********************************************************************************************
Notes: 

Route Config points to /register
Controller Retrieves a message and returns it to the registration page, the page also returns an alert when you click on the register button, the page itself uses local validation to ensure that the passwords are the same and that the username is not too long or too short, also all input fields are required 
